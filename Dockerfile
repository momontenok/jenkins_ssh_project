FROM jenkins/jenkins:lts

USER root

ENV DOCKER_DIR="/home/jenkins/"

RUN apt-get update && \
    apt-get install -y default-jdk default-jre && \
    apt-get install -y gcc g++ openssh-server rsync && \
    mkdir /var/run/sshd \
    && echo 'root:password' | chpasswd \
    && echo 'jenkins:password' | chpasswd \
    && sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config \
    && sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

USER jenkins

CMD ["/usr/bin/java", "-jar", "/usr/share/jenkins/jenkins.war"]


