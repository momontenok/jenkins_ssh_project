pipeline {

    agent any

    stages{

        stage('Build Project') {
            steps{
                script {
                    def commitMessage = sh(script: 'git log --format=%B -n 1', returnStdout: true).trim()
                    def version = '0.0.0'
                    if (commitMessage) {
                        def matcher = (commitMessage =~ /(\d+\.\d+\.\d+)/)
                        if (matcher) {
                            version = matcher[0][0]
                        }
                    }
                    str1 = 'sshpass -p nvidia ssh -o StrictHostKeyChecking=no nvidia@10.8.0.3 " if [ -d "iqfill_full_firmware_builder" ]; then cd iqfill_full_firmware_builder && chmod +x builder; else git clone https://gitlab.com/agdtpro/computer_vision/product/iqfill/iqfill_full_firmware_builder.git && cd iqfill_full_firmware_builder && chmod +x builder; fi && source /opt/ros/melodic/setup.bash; "./builder -v '
                    str2 = ' -e resunet4dlv3plus_db240703_ft240423from80.engine""'
                    def fullCommand = str1 + version + str2
                    sh(script: fullCommand)
                }
            }
        }

    }
}